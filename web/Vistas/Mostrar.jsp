<%-- 
    Document   : Listar
    Created on : 24/11/2022, 10:43:21 AM
    Author     : estudiante
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="Modelo.Persona"%>
<%@page import="ModeloDAO.PersonaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

        <title>JSP Page</title>
    </head>
    <body>
        
        <div class="container">
            <h1>Personas</h1>
            <a class="btn btn-primary" href="controlador.po?accion=Agregar">Agregar persona</a>
            <br>
            <br>

            <table class="table table-bordered" >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <%
                    PersonaDAO dao = new PersonaDAO();
                    List<Persona> list = dao.Mostrar();
                    Iterator<Persona> iter = list.iterator();
                    Persona per = null;
                    while (iter.hasNext()) {
                        per = iter.next();


                %>
                <tbody>
                    <tr>
                        <td><%= per.getId()%></td>
                        <td><%=per.getNombre()%></td>
                        <td><%=per.getApellido()%></td>
                        <td class=" text-center"> 
                            <a class="btn btn-info" href="controlador.po?accion=Editar&id=<%= per.getId()%>">Editar</a>
                            <a class="btn btn-danger" href="controlador.po?accion=Eliminar&id=<%= per.getId()%>">Eliminar</a>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
        </div>
    </body>
</html>
