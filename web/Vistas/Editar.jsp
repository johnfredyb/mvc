<%-- 
    Document   : Editar
    Created on : 24/11/2022, 10:43:39 AM
    Author     : estudiante
--%>

<%@page import="Modelo.Persona"%>
<%@page import="ModeloDAO.PersonaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
              PersonaDAO dao=new PersonaDAO();
              int id=Integer.parseInt((String)request.getAttribute("idper"));
              Persona p=(Persona)dao.list(id);
          %>
        
        <h1>Editar Persona</h1>

        <form action="controlador.po">
            
            <div class="mb-3">
                <label for="Nombre" class="form-label">Nombre</label>
                <input type="text" class="form-control" name="txtNombre" value="<%= p.getNombre() %>">
            </div>
            <div class="mb-3 ">
                <label for="Apellido" class="form-label">Apellido</label>
                <input type="text" class="form-control" name="txtApellido" value="<%= p.getApellido()%>">
                <input type="hidden" name="txtId" value="<%= p.getId()%>">
            </div>
            <button type="submit" name="accion" value="Actualizar" class="btn btn-primary">Actualizar</button>
        </form>
    </body>
</html>
