<%-- 
    Document   : Add
    Created on : 24/11/2022, 10:43:54 AM
    Author     : Fredy Buitrago
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Agregar Persona</h1>

        <form action="controlador.po">
            
            <div class="mb-3">
                <label for="Nombre" class="form-label">Nombre</label>
                <input type="text" class="form-control" name="txtNombre">
            </div>
            <div class="mb-3 ">
                <label for="Apellido" class="form-label">Apellido</label>
                <input type="text" class="form-control" name="txtApellido">
            </div>
            <button type="submit" name="accion" value="agrega" class="btn btn-primary">Agregar</button>
        </form>
    </body>
</html>
