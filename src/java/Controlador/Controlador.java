/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Persona;
import ModeloDAO.PersonaDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author estudiante
 */
public class Controlador extends HttpServlet {

    String Mostrar = "Vistas/Mostrar.jsp";
    String Agregar = "Vistas/Agregar.jsp";
    String Editar = "Vistas/Editar.jsp";
    Persona p = new Persona();
    PersonaDAO dao = new PersonaDAO();
    int id;
    // Persona p=new Persona();//instanciamos la clase persona
    //PersonaDAO dao= new PersonaDAO();// tambien instanciamos el DAO

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controlador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acceso = "";
        String action = request.getParameter("accion");
        if (action.equalsIgnoreCase("Mostrar")) {
            acceso = Mostrar;
        } else if (action.equalsIgnoreCase("Agregar")) {
            acceso = Agregar;  // nos lleva a la ruta de la vista agregar

        } else if (action.equalsIgnoreCase("agrega")) {//valor que nos envia el boton agregar

            String nombre = request.getParameter("txtNombre");
            String apellido = request.getParameter("txtApellido");
            //envia los datos que se van a adregar al dao
            p.setNombre(nombre);
            p.setApellido(apellido);
            dao.Agregar(p);//agrega los elemento a el dao y a la BD
            // vuelve a la vista mostrar
            acceso = Mostrar;
        } else if (action.equalsIgnoreCase("Editar")) {
            request.setAttribute("idper", request.getParameter("id"));
            acceso = Editar;
        } else if (action.equalsIgnoreCase("Actualizar")) {
            id = Integer.parseInt(request.getParameter("txtId"));
            String nombre = request.getParameter("txtNombre");
            String apellido = request.getParameter("txtApellido");
            //envia los datos que se van a adregar al dao
            p.setId(id);
            p.setNombre(nombre);
            p.setApellido(apellido);
            dao.Editar(p);
            acceso = Mostrar;

        }else if (action.equalsIgnoreCase("Eliminar")) {
            id = Integer.parseInt(request.getParameter("id"));
            p.setId(id);
            dao.Eliminar(id);
            acceso=Mostrar;
        }

        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
