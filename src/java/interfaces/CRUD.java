
package interfaces;

import Modelo.Persona;
import java.util.List;


public interface CRUD {
    
    public List Mostrar();
    public Persona list(int id);
    public boolean Agregar(Persona per);
    public boolean Editar(Persona per);
    public boolean Eliminar(int id);
    
    
}
